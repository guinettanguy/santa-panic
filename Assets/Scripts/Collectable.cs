using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    private void OnTriggerEnter3D(Collider collision)
    {
        if (collision.CompareTag("Player"))
        {
            Inventory.instance.AddPresents(1);
            Destroy(gameObject);
        }

    }
}