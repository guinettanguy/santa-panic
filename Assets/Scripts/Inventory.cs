using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Inventory : MonoBehaviour
{
    public int presentsCount;
    public Text presentsCountText;
    public static Inventory instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("More than one instance in Inventory");
            return;
        }
        instance = this;
    }
    public void AddPresents(int count)
    {
        presentsCount += count;
        presentsCountText.text = presentsCount.ToString();

    }
}